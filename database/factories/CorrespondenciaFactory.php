<?php

namespace Database\Factories;

use App\Models\Correspondencia;

use App\Models\Destinatario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Correspondencia>
 */
class CorrespondenciaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'fecha' => $this->faker->date,
            'remitente' => $this->faker->company,
            'asunto' => $this->faker->sentence,
            'cite' => $this->faker->word,
            'destinatario_id' => Destinatario::inRandomOrder()->first()->id, // Selecciona una editorial existente

        ];
    }
}
