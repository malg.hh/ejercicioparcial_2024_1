<?php

namespace Database\Seeders;

use App\Models\Correspondencia;
use App\Models\Destinatario;


use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CorrespondenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $destinatarios = Destinatario::all();

        // Crear 100 correspondencias, asignando un destinatario aleatorio a cada una
        Correspondencia::factory()->count(100)->make()->each(function($correspondencia) use ($destinatarios) {
            $correspondencia->destinatario_id = $destinatarios->random()->id;
            $correspondencia->save();
        });
    }
}
