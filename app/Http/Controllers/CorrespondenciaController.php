<?php

namespace App\Http\Controllers;

use App\Models\Correspondencia;
use App\Models\Destinatario;
use Illuminate\Http\Request;

class CorrespondenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $correspondencias = Correspondencia::all();
        return view('correspondencias.index', [
            'correspondencias' => $correspondencias
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $destinatarios = Destinatario::all();
        return view('correspondencias.create', [
            'destinatarios' => $destinatarios
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Correspondencia::create($request->all());
        return redirect()->route('correspondencias.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Correspondencia $correspondencia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Correspondencia $correspondencia)
    {
        $destinatarios = Destinatario::all();
        return view('correspondencias.edit', [
            'correspondencia' => $correspondencia,
            'destinatarios' => $destinatarios
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Correspondencia $correspondencia)
    {
        $correspondencia->update($request->all());
        return redirect()->route('correspondencias.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Correspondencia $correspondencia)
    {
        $correspondencia->delete();
        return redirect()->route('correspondencias.index');
    }
}
