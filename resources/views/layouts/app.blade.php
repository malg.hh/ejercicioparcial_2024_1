<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Laravel App</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">


    <link rel="stylesheet" href="{{ asset('stilos/styles.css') }}">

    <style>

    </style>
    <script defer>
        function toggleMenu() {
            const menu = document.getElementById('mobile-menu');
            menu.classList.toggle('hidden');
        }
    </script>
</head>
<body >

    <!-- Header -->
    <header class="bg-blue-400 shadow-md p-4 mb-4 flex justify-between items-center">
        <div class="relative">
            <!-- Hamburger Menu Button -->
            <button onclick="toggleMenu()" class="text-gray-800 focus:outline-none lg:hidden">
                <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"></path>
                </svg>
            </button>

            <!-- Mobile Menu -->
            <div id="mobile-menu" class="hidden absolute bg-white shadow-lg rounded-lg mt-2 w-48 lg:block lg:relative lg:mt-0 lg:bg-transparent lg:shadow-none lg:rounded-none lg:flex lg:items-center">
                <ul class="flex flex-col lg:flex-row lg:space-x-4">
                    <li><a href="/" class="block py-2 px-4 text-gray-800 hover:bg-gray-200">Home</a></li>
                    <li><a href="{{ route('correspondencias.index') }}" class="block py-2 px-4 text-gray-800 hover:bg-gray-200">Correspondencia</a></li>
                    <li><a href="{{ route('destinatarios.index') }}" class="block py-2 px-4 text-gray-800 hover:bg-gray-200">Destinatarios</a></li>

                </ul>
            </div>
        </div>
        <h1 class="text-4xl font-bold text-white text-shadow">EXAMEN SEGUNDO PARCIAL </h1>
        <div>
            <h2 class="text-lg font-semibold">Bienvenido</h2>
        </div>
    </header>

    <!-- Main Content Area -->
    <div class="bg-white p-6 shadow-md flex-grow overflow-auto">
        @yield('content')
    </div>


</body>
</html>
