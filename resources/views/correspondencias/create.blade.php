@extends('layouts.app')

@section('content')
<div class="flex justify-center items-center flex-col w-full h-auto rounded-lg">
    <div class="w-1/2 flex justify-center items-center flex-col rounded-lg border border-blue-500">
        <div class="w-1/2">
            <form action="{{ route('correspondencias.store') }}" method="post">
                @csrf

                <br>





                <label for="fecha" class="block text-sm font-medium leading-6 text-gray-900">Fecha</label>
                <div class="mt-2">
                    <input id="fecha" name="fecha" type="date" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                </div>

                <div>
                    <label for="remitente" class="block text-sm font-medium leading-6 text-gray-900">remitente</label>
                    <div class="mt-2">
                        <input id="remitente" name="remitente" type="text" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>

                <div>
                    <label for="asunto" class="block text-sm font-medium leading-6 text-gray-900">asunto</label>
                    <div class="mt-2">
                        <input id="asunto" name="asunto" type="text" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>


                <div>
                    <label for="cite" class="block text-sm font-medium leading-6 text-gray-900">cite</label>
                    <div class="mt-2">
                        <input id="cite" name="cite" type="text" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>

                <div>
                    <label for="destinatario_id" class="block text-sm font-medium leading-6 text-gray-900">Editorial</label>
                    <div class="mt-2">
                        <select id="destinatario_id" name="destinatario_id" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                            @foreach ($destinatarios as $destinatario)
                                <option value="{{ $destinatario->id }}">{{ $destinatario->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="mt-4">
                    <button type="submit" class="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Registrar Correspondencia</button>
                </div>
            </form>
            <br>
        </div>
    </div>
</div>
@endsection
