@extends('layouts.app')

@section('content')

    <table class="min-w-full divide-y divide-gray-200 dark:divide-gray-700">
        <thead>
            <tr>

                <th class="px-6 py-3 bg-gray-50 dark:bg-gray-800">ID</th>
                <th class="px-6 py-3 bg-gray-50 dark:bg-gray-800">nombre</th>
                <th class="px-6 py-3 bg-gray-50 dark:bg-gray-800">cargo</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($destinatarios as $destinatario)
            <tr>
                <td class="px-6 py-4 whitespace-nowrap">{{ $destinatario->id }}</td>
                <td class="px-6 py-4 whitespace-nowrap">{{ $destinatario->nombre }}</td>
                <td class="px-6 py-4 whitespace-nowrap">{{ $destinatario->cargo }}</td>

                <td class="px-6 py-4 whitespace-nowrap">
                    <a class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" href="{{ route('destinatarios.edit', $destinatario->id) }}">Editar</a>
                    <form action="{{ route('destinatarios.destroy', $destinatario->id) }}" method="POST" style="display:inline-block;">
                        @csrf
                        @method('DELETE')
                        <input class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded" type="submit" value="Eliminar">
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <a href="{{ route('correspondencias.create') }}" class="mt-4 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">Insertar Libro</a>

@endsection
